import { WeChat_CurrentUserKey } from '../../../constants'
import { MessageInfo, MessageInfoModel, MessageTypeEnum } from '../../../models/message'
import { UserInfo, UserInfoModel } from '../../../models/users'
import { AudioRenderer } from '../../../utils/audio_renderer'
import { FileCommon } from '../../../utils/file_operate'
import { VoiceTransfer } from '../../../utils/VoiceTransfer'

export interface PopupItem {
  title: string
  icon: ResourceStr
  itemClick?: () => void
}
@Preview
@Component
struct Message {
  @StorageProp(WeChat_CurrentUserKey)
  currentUser: UserInfoModel = new UserInfoModel({} as UserInfo)
  @Prop
  currentMessage: MessageInfoModel = new MessageInfoModel({} as MessageInfo)
  @State
  isOwnMessage: boolean = this.currentUser.user_id === this.currentMessage?.sendUser?.user_id
  @State
  showPopup: boolean = false // 是否显示浮层
  @State
  audioState: AnimationStatus = AnimationStatus.Initial
  @State transferResult:string=''
  // 删除信息的方法
  delMessage: (messId: string) => void = () => {}
  // 图片预览
  previewImage: () => void = () => {}
  videoController: VideoController = new VideoController() // 视频controller
  // 获取文本消息
  @Builder
  getTextContent() {
    Text(this.currentMessage.messageContent)
      .backgroundColor(this.isOwnMessage ? $r('app.color.second_primary') : $r('app.color.white'))
      .fontColor($r('app.color.text_primary'))
      .padding(10)
      .lineHeight(24)
      .margin({
        left: 10,
        right: 10
      })
      .borderRadius(5)
  }
  // 获取图片显示
  @Builder
  getImageContent() {
    Column() {
      Image("file://" + this.currentMessage.sourceFilePath)
        .borderRadius(4)
        .width('100%')
        .enabled(false)
    }
    .width('40%')
    .margin({
      left: 10,
      right: 10
    })
    .onClick(() => {
      this.previewImage()
    })
  }
  // 获取语音宽度
  getAudioWidth () {
    let minWidth: number = 20
    let maxWidth: number = 90
    let calWidth: number = minWidth + (100 * this.currentMessage.sourceDuration / 60)
    if(calWidth > maxWidth) return maxWidth+"%"
    return calWidth+"%"
  }
  // 获取语音显示
  @Builder
  getAudioContent() {
    Column({ space: 10 }) {
      Row({ space: 5 }) {
        Text(`${this.currentMessage.sourceDuration}'`)
          .textAlign(TextAlign.Center)
        ImageAnimator()
          .images([
            {
              src: $r("app.media.ic_public_voice3")
            },
            {
              src: $r("app.media.ic_public_voice1")
            },
            {
              src: $r("app.media.ic_public_voice2")
            },
            {
              src: $r("app.media.ic_public_voice3")
            }
          ])
          .iterations(-1)
          .state(this.audioState)
          .width(20)
          .height(20)
          .rotate({
            angle: this.isOwnMessage ? 180 : 0
          })
      }
      .width(this.getAudioWidth())
      .backgroundColor(this.isOwnMessage ? $r('app.color.chat_primary') : $r('app.color.white'))
      .justifyContent(this.isOwnMessage ? FlexAlign.End : FlexAlign.Start)
      .height(40)
      .padding({
        left: 10,
        right: 10
      })
      .margin({
        left: 10,
        right: 10
      })
      .borderRadius(4)
      .direction(this.isOwnMessage ? Direction.Ltr : Direction.Rtl)
      .onClick(() => {
        AudioRenderer.start(this.currentMessage.sourceFilePath, () => {
          this.audioState = AnimationStatus.Stopped
        })
        this.audioState = AnimationStatus.Running
      })
      if(this.transferResult) {
        Text(this.transferResult)
          .backgroundColor(this.isOwnMessage ? $r('app.color.second_primary') : $r('app.color.white'))
          .fontColor($r('app.color.text_primary'))
          .padding(10)
          .lineHeight(24)
          .margin({
            left: 10,
            right: 10
          })
          .borderRadius(5)
      }
    }.alignItems(this.isOwnMessage ? HorizontalAlign.End : HorizontalAlign.Start)

  }
  @State
  popupList: PopupItem[] = [
    {
    title: '听筒播放',
    icon: $r("app.media.ic_public_ears")
  },
    {
      title: '收藏',
      icon: $r("app.media.ic_public_cube")
    }, {
      title: '转文字',
      icon: $r("app.media.ic_public_trans_text"),
    itemClick: () => {
      if(this.currentMessage.messageType === MessageTypeEnum.AUDIO) {
        VoiceTransfer.VoiceToText(this.currentMessage.sourceFilePath, (res) => {
          this.transferResult = res.result
        })
      }
      this.showPopup = false
    }
    }, {
      title: '删除',
      icon: $r("app.media.ic_public_cancel"),
      itemClick: () => {
        if(this.currentMessage.sourceFilePath){
          FileCommon.delFilePath(this.currentMessage.sourceFilePath)
        }
        this.delMessage(this.currentMessage.id)
      }
    }, {
      title: '多选',
      icon: $r("app.media.ic_public_multi_select")
    }, {
      title: '引用',
      icon: $r("app.media.ic_public_link")
    }, {
      title: '提醒',
      icon: $r("app.media.ic_public_warin")
    }]
  // 用来显示弹层的内容

  @Builder
  getContent() {
    GridRow ({ columns: 5 }) {
      ForEach(this.popupList, (item: PopupItem) => {
        GridCol() {
          Column({ space: 6 }) {
            Image(item.icon)
              .fillColor($r("app.color.white"))
              .width(18)
              .height(18)
            Text(item.title)
              .fontColor($r("app.color.white"))
              .fontSize(14)
          }
          .height(60)
        }
        .onClick(() => {
          item.itemClick && item.itemClick() // 如果点击事件存在 就触发
        })
      })
    }
    .width(300)
    .padding({
      top: 15,
      left: 10
    })
  }
  @Builder
  getVideoContent() {
    Column() {
      Stack() {
        Video({
          src: "file://" + this.currentMessage.sourceFilePath,
          controller: this.videoController
        })
          .controls(false)
          .width(100)
          .height(150)
          .borderRadius(4)
          .id(this.currentMessage.id)
          .onPrepared(async () => {
            this.videoController.setCurrentTime(0.1) // 拨到0.1秒
          })
        Image($r("app.media.ic_public_play"))
          .width(30)
          .height(30)
          .fillColor($r("app.color.back_color"))
      }
    }
    .margin({
      left: 10,
      right: 10
    })
    .onClick(() => {
      this.previewImage()
    })
  }

  build() {
    Row() {
      Column(){
        Image(this.currentMessage.sendUser?.avatar)
        .height(40)
        .width(40)
        .borderRadius(6)
      }
      Row() {
        Column() {
          Column() {
            if (this.currentMessage.messageType === MessageTypeEnum.TEXT) {
              this.getTextContent()
            } else if (this.currentMessage.messageType === MessageTypeEnum.AUDIO) {
              this.getAudioContent()
            }else if(this.currentMessage.messageType === MessageTypeEnum.IMAGE) {
              // 照片消息
              this.getImageContent()
            }
            else if (this.currentMessage.messageType === MessageTypeEnum.VIDEO) {
              // 视频消息
              this.getVideoContent()
            }
          }
            .bindPopup(this.showPopup, {
              builder:  this.getContent,
              popupColor: $r("app.color.popup_back"),
              backgroundBlurStyle: BlurStyle.NONE,
              onStateChange: (event) => {
                // 当手指点了其他位置 关闭状态
                this.showPopup = event.isVisible
              }
            })
        }
        .gesture(
          LongPressGesture()
            .onAction(() => {
              this.showPopup = true // 显示父层
            }))
      }
      .layoutWeight(6)
      .justifyContent(this.isOwnMessage ? FlexAlign.End : FlexAlign.Start)
      Text()
        .layoutWeight(1)
    }
    .width('100%')
    .padding({
      left: 20,
      right: 20
    })
    .alignItems(VerticalAlign.Top)
    .direction(this.isOwnMessage ? Direction.Rtl : Direction.Ltr)
  }
}

export default Message